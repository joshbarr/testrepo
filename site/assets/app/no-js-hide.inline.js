(function(document, window) {
    var css = ".js-hide{display:none;}";
    var root = document.documentElement;
    var head = document.head || document.getElementsByTagName('head')[0];
    var style = document.createElement("style");
    style.type = 'text/css';
    if (style.styleSheet){
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    head.appendChild(style);
    root.className = root.className.replace(/no-js$/, "js");
})(document, window);