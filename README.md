# test app



### CSS Precompilation

```
gulp css
```


### JS

Powered by browerify.

```
gulp js
```

### Icons

Build SVG + PNG fallback icons

```
gulp icon
```


### Watchers

Watch CSS, JS, SVGs

```
gulp watch
```


### Testing
Write your tests using Mocha and Chai in the `specs` directory.

Test the app with `gulp test`

