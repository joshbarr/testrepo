/**
 * ----------------------------------------------------------------------------
 * Deps
 * ----------------------------------------------------------------------------
 */

var gulp = require('gulp');
var fs = require('fs');
var del = require('del');
var sass = require('gulp-sass');
var path = require("path");
var plumber = require('gulp-plumber');
var gulpBrowserify = require('gulp-browserify');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var glob = require('glob');
var jshint = require('gulp-jshint');
var grunt = require('gulp-grunt')(gulp);
var concat = require('gulp-concat');
var _ = require("underscore");
var gutil = require("gulp-util");
var rename = require("gulp-rename");
var glob = require('glob');
var rimraf = require('gulp-rimraf');
var source = require('vinyl-source-stream');


/**
 * ----------------------------------------------------------------------------
 * Config
 * ----------------------------------------------------------------------------
 */

var paths = {
    appName: "test-app.js",
    build: "./build/",
    dist: "./www/assets",
    js: "./site/assets/app",
    slug: "test-app",
    spec: "./spec",
    src: "./site/assets",
    test: "./test",
    views: "./site/views",
    www: "www"
};

var templateFileExt = "html";
var rootTemplateName = "index.tpl";


/**
 * ----------------------------------------------------------------------------
 * CSS
 * ----------------------------------------------------------------------------
 */

gulp.task('css', ['clean'], function (done) {
    var postcss      = require('gulp-postcss');
    var sourcemaps   = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer-core');

    gulp.src(path.join(paths.src, "sass", "**", "*.scss"))
        .pipe(sourcemaps.init())
        .pipe(sass({
            sourceComments: 'none'
        }))
        .pipe(postcss( [autoprefixer] ))
        .pipe(gulp.dest(paths.dist))
        .pipe(sourcemaps.write('.'))

    done();
});


gulp.task('clean', function() {
    return del([
        path.join(paths.dist, "**/*.js"),
        path.join(paths.dist, "**/*.css"),
        path.join(paths.dist, "icons"),
        path.join(paths.test, "*.js"),
        path.join(paths.test, "*.css"),
        path.join(paths.spec, "all.js")
    ]);
});


/**
 * ----------------------------------------------------------------------------
 * Icons
 * ----------------------------------------------------------------------------
 *
 * This is a grunt task. The gulp-grunt bridge lets us run these with a grunt-
 * prefix, so grunt-clean would run clean from your Gruntfile.
 *
 * mind === blown
 */

gulp.task('icon', ['grunt-icon'], function() {

});


/**
 * ----------------------------------------------------------------------------
 * Watching
 * ----------------------------------------------------------------------------
 */

gulp.task("watch", function() {
    gulp.watch(path.join( paths.src, "sass/**/*.scss"), ["css"]);
    gulp.watch(path.join( paths.src, "**/*.js"), ["js"]);
    gulp.watch(path.join( paths.src, "svg/**/*.svg"), ["icon"]);
    gulp.watch(path.join( paths.src, "**/*.inline.js"), ["critical"]);
    gulp.watch(path.join( paths.src, "**/*.inline.js"), ["critical"]);
    gulp.watch(path.join( paths.views, "**/*.tpl"), ["critical"]);
});




/**
 * ----------------------------------------------------------------------------
 * Frontend JS
 * ----------------------------------------------------------------------------
 */

gulp.task('js', ['clean'], function(done) {
    var jsPath = path.join(paths.dist, "js");

    // Run a bundle through browserify
    var bundleThis = function(srcArray) {
        _.each(srcArray, function(src) {
            var localName = src.replace(".entry.js", ".js");
            localName = path.relative(paths.js, localName);

            var bundle = browserify([ "./" + src ]).bundle();
            gutil.log("Browserifying", src);
            bundle.pipe(source(localName))
                  .pipe(gulp.dest( jsPath ));
        });
    };

    var entryPoints = glob.sync( path.join(paths.js, "**/*.entry.js") );

    if (entryPoints) {
        bundleThis(entryPoints);
    }

    // Copy non-bundle files
    gulp.src([
        path.join(paths.js, "**", "*.js"),
        "!" + path.join(paths.js, "**", "_*.js"),
        "!" + path.join(paths.js, "**", "*.entry.js"),
        "!" + path.join(paths.js, "components", "**", "*.js"),
        "!" + path.join(paths.js, "lib", "**", "*.js")
    ])
    .pipe(gulp.dest( jsPath ))
    .on("end", function() {
        done();
    })
});




gulp.task("dist", ['js', 'css', 'icon'], function(done) {
    var minifyCSS = require('gulp-minify-css');

    gulp.src( path.join(paths.dist, "**", "*.js" ))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist))

    gulp.src( path.join(paths.dist, "**", "*.css"))
        .pipe(minifyCSS({
            keepBreaks: false,
            debug: false,
            noRebase: true
        }))
        .pipe(gulp.dest(paths.dist))

    done();
});



gulp.task('default', ["critical", "icon", "dist"], function(done) {
    done();
});




/**
 * ----------------------------------------------------------------------------
 * Inline critical assets
 * ----------------------------------------------------------------------------
 */

gulp.task('critical', ['js', 'css', 'icon'], function (done) {
    var inlinesource = require('gulp-inline-source');
    var filename = rootTemplateName;
    var newname = filename.replace(/tpl$/, templateFileExt);

    gulp.src(path.join(paths.views, filename))
        .pipe(inlinesource(paths.dist))
        .pipe(rename(newname))
        .pipe(gulp.dest(paths.views))
        .on("end", function() {
            var inlineJs = path.join(paths.dist, "**", "*.inline.js");
            var inlineCss = path.join(paths.dist, "**", "*.inline.css");
            del.sync(inlineJs);
            del.sync(inlineCss);
            done();
        })
});






/**
 * ----------------------------------------------------------------------------
 * Test setup
 * ----------------------------------------------------------------------------
 */

var mochaPhantomJS = require('gulp-mocha-phantomjs');

gulp.task('spec', ['clean', 'critical'], function() {
    del("test/spec.js");

    return gulp.src(path.join(paths.spec, "**/*.spec.js"))
        .pipe(concat('all.js'))
        .pipe(gulp.dest('spec'))
        .on('end', function() {

            var bundleStream = browserify({
                extensions: ['.coffee', '.nunj'],
                debug: true,
                entries: [ "./spec/all.js" ]
            }).bundle();

            return bundleStream
                .pipe(source('spec.js'))
                .pipe(gulp.dest(paths.test))
                .on("end", function() {
                    del("spec/all.js");
                })
        });
});




function startExpress(_port) {
    var express = require("express");
    var app = express();

    // Serves main page
    app.get("/", function(req, res) {
        res.sendFile( path.join( __dirname. paths.test, 'test.html'));
    });

    app.use("/assets", express.static(__dirname + "/" + paths.www + "/assets"));
    app.use(express.static(__dirname +  "/" + paths.test));

    var port = process.env.PORT || _port;
    return app.listen(port, function() {
        console.log("Running tests in PhantomJS on port " + port);
    });
}


gulp.task('test-harness', ['js'], function() {
    return gulp.src([
        "node_modules/mocha/mocha.js",
        "node_modules/chai/chai.js",
        "node_modules/sinon/pkg/sinon.js",
        "node_modules/mocha/mocha.css"
    ])
    .pipe(gulp.dest("test"));
});


gulp.task('clean-mocha', function() {
     try {
        del("mocha.json");
    } catch(e) {

    }
});

gulp.task('test', ['spec', 'test-harness', 'clean-mocha'], function () {
    var port = 5000;
    var appRef = startExpress(port);

    // Close the server when we're finished with it.
    function handleEnd() {
        appRef.close();
    }

    var phantomConfig = {
        viewportSize: {
            width: 1180,
            height: 768
        }
    }



    var testConfig = {
        dev: {
            phantomjs: phantomConfig
        },
        ci: {
            phantomjs: phantomConfig,
            reporter: "./bamboo-reporter.js",
            dump: "mocha.json"
        }
    };

    var whichConfig = testConfig.dev;

    if (gutil.env.ci) {
        whichConfig = testConfig.ci;
    }

    var stream = mochaPhantomJS(whichConfig);


    stream.write({
        path: 'http://localhost:' + port + '/test.html',
    });

    stream.on("end", handleEnd);
    stream.on("err", handleEnd);
    stream.end();
    return stream;

});

