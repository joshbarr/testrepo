module.exports = function(grunt) {
    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            js: "build"
        },

        grunticon_pigment: {
            app: {
                files: [{
                    cwd: 'site/assets/svg',
                    dest: 'www/assets/icons'
                }],
                options: {
                    svgFolder: "./",
                    "svgColorFolder": "colourise",
                    defaultWidth: "32px",
                    defaultHeight: "32px",
                    tmpDir: "build",
                    // primary, white, light blue, dark grey, light grey
                    svgColors: [
                        "#014781",
                        "#ffffff",
                        "#7d9bb4",
                        "#535456",
                        "#9d9fa1",
                        "#50a518",
                        "#c4dfb2",
                        "#56af1b"
                    ]
                }
            }
        }
    });

    /**
     * The cool way to load your grunt tasks
     * --------------------------------------------------------------------
     */
    Object.keys( pkg.devDependencies ).forEach( function( dep ){
        if( dep.substring( 0, 6 ) === 'grunt-' ) grunt.loadNpmTasks( dep );
    });


    grunt.registerTask("icon", [
        "grunticon_pigment:app"
    ]);

};