describe("Critical", function() {
    var critical = require("../site/assets/app/critical.inline");

    it("Should init the GA tracking code", function() {
        setTimeout(function () {
            should.exist(ga);
        }, 600);
    });

    it("Should implement grunticon", function() {
        should.exist(window.grunticon);
        expect(window.grunticon).to.be.a('function');
    });

    it("Should load grunticon CSS files", function() {
        setTimeout(function () {
            var css = document.querySelector('link[href="/assets/icons/icons-data-svg.css"]');
            should.exist(css);
        }, 400);
    });
});


describe("test-app.js", function() {
    var app = require("../site/assets/app/test-app.entry");

    it("Should init the application", function() {
        should.exist(app);
    });
});