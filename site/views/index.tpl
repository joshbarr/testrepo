<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6 no-js" lang="en"><![endif]-->
<!--[if IE 7]> <html class="ie7 no-js" lang="en"><![endif]-->
<!--[if IE 8]> <html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]> <html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <meta name='keywords' content=''>
    <meta name='description' content=''>

    <!-- open graph -->
    <meta property="og:url" content="" />
    <meta property="og:title" content="test app" />
    <meta property="og:image" content="/logo.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="400">
    <meta property="og:image:height" content="400">
    <!-- end:features -->

    <!-- google-plus -->
    <link href="https://plus.google.com/[[id]]" rel="publisher"/>
    <!-- end:google-plus-->

    <!-- features -->
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <meta content="telephone=no" name="format-detection">
    <!-- end:features -->

    <title>test app</title>

    <link type="text/plain" rel="author" href="/humans.txt" />

    <!-- icons -->
    <link rel='shortcut icon' href='/favicon.ico'>
    <link rel='apple-touch-icon-precomposed' sizes='76x76' href='/apple-touch-icon-ipad.png'>
    <link rel='apple-touch-icon-precomposed' sizes='120x120' href='/apple-touch-icon-iphone-retina.png'>
    <link rel='apple-touch-icon-precomposed' sizes='152x152' href='/apple-touch-icon-ipad-retina.png'>
    <link rel='apple-touch-icon-precomposed' href='/apple-touch-icon-precomposed.png'>
    <!-- end:icons -->

    <!-- styles -->
    <link rel="stylesheet" href="critical.inline.css" inline>
    <link rel="stylesheet" href="/assets/screen.css" />
    <!-- end:styles -->

    <!-- js-support -->
    <noscript>
        <style type='text/css'>
            .no-js-hide { display: none !important; }
        </style>
    </noscript>
    <script type="text/javascript" src="no-js-hide.inline.js" inline></script>
    <!-- end:js-support -->

</head>
<body>

<h1>test app</h1>
<p>Hey, it works!</p>
<p>Slug: test-app</p>

<h2>Next steps</h2>
<p>
    Run slush springroll:css to add some CSS
</p>


<script src="/assets/js/test-app.js"></script>
<script src="js/critical.inline.js" inline></script>


</body>
</html>